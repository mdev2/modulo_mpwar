class mpwar{
class {'apache':}

apache::vhost { 'centos.dev':
	port	=> '80',
	docroot	=> '/var/www/',
	docroot_owner	=> 'vagrant',
	docroot_group => 'vagrant',
}

apache::vhost { 'project1.dev':
	port	=> '80',
	docroot	=> '/var/www/project1/',
	docroot_owner	=> 'vagrant',
	docroot_group => 'vagrant',
}


class {'mysql':}

mysql::grant{ 'mpwar_test':
	mysql_privileges => 'ALL',
	mysql_db => 'mpwar_test',
	mysql_user => 'mpwar_test',
	mysql_password => 'pwd',
	mysql_host => '127.0.0.1'
}

class {'php':}


class {'iptables':}

iptables::rule{ 'http':
	port => '80',
	protocol => 'tcp'
}
iptables::rule{ 'http2':
	port => '8080',
	protocol => 'tcp'
}


class {'hosts':}

host {'localhost':
	ensure => present,
	target => '/etc/hosts',
	ip => '127.0.0.1',
	host_aliases => ['mysql1', 'memcached1']
}




class { 'yum':
  extrarepo => 'epel',
}


class {'memcached':}
}
